upstream gogs {
    server 127.0.0.1:6000;
}

server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name _;

    location ^~ /.well-known/acme-challenge/ {
        access_log off;
        log_not_found off;

        proxy_pass {{ acme_forward_protocol }}acme;

        proxy_http_version 1.1;

        # Mitigating the HTTPoxy Vulnerability
        proxy_set_header Proxy "";

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    return 301 https://$host$request_uri;
}

server {
    listen 443 default_server ssl http2;
    listen [::]:443 default_server ssl http2;

    ssl_certificate /etc/ssl/certs/host.pem;
    ssl_certificate_key /etc/ssl/private/host.key;

    server_name _;

    client_max_body_size 50M;

    location ^~ /.well-known/acme-challenge/ {
        access_log off;
        log_not_found off;

        proxy_pass {{ acme_forward_protocol }}acme;

        proxy_http_version 1.1;

        # Mitigating the HTTPoxy Vulnerability
        proxy_set_header Proxy "";

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location / {
        proxy_pass http://gogs;

        proxy_http_version 1.1;

        # Mitigating the HTTPoxy Vulnerability
        proxy_set_header Proxy "";

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location /explore {
        return 301 /;
    }

    location /api/v1/users/search {
        return 404;
    }
}
